void paint() {
  if (ded)paintDed();
  else {
    bGround();
    mGround();
    fGround();
  }
}
void paintDed(){
  fill(255);
  background(0);
  textSize(85);
  text("you are dead, not big surprise", width/2,height/2);
}
void bGround() {
  //background(124, 71, 0);
  image(table, centr.x, centr.y);
  //pixlPaint(table, centr.x, centr.y, width, height);
}
void mGround() {
  lpaint();
  for (int i = D.size()-1; i >= 0; i--)D.get(i).paint();
  S.paint();
}
void fGround() {
  if (lomp) {
    //pixlPaint(shade, centr.x, centr.y, width/2, height/2);
    image(shade, centr.x, centr.y);
  }
  image(lamp, centr.x, centr.y);
}
void lpaint() {
  for (PVector lp : lockPoint) {
    noFill();
    stroke(0);
    rect(lp.x, lp.y, D.get(0).wh.x, D.get(0).wh.y);
  };
}

void pixlPaint(PImage img, PVector xy, PVector wh) {
  pixlPaint(img, xy.x, xy.y, wh.x, wh.y);
}
void pixlPaint(PImage img, float x, float y, float w, float h) {
  //noStroke();
  strokeWeight(1);
  int ind = 0;
  float
    pw = (w/img.width),
    ph = (h/img.height),
    ofstX = (x+(1-img.width)*pw/2),
    ofstY = (y+(1-img.height)*ph/2);
  for (float j = ofstY; j < h+ofstY; j+=ph)for (float i = ofstX; i < w+ofstX-1 && ind < img.pixels.length; i+=pw) {
    color c = img.pixels[ind];
    fill(c, alpha(c));
    stroke(c, alpha(c));
    rect(i, j, pw, ph);
    ind++;
  }
}
void colr(PImage im, color c) {
  im.loadPixels();
  for (int i = 0; i < im.pixels.length; i++)im.pixels[i]=color(red(c), blue(c), green(c), alpha(c));
  im.updatePixels();
}
