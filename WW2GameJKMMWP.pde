int hold = -1;
PVector mus = new PVector(), centr;
PVector[] lockPoint = new PVector[2];
ArrayList<Document> D = new ArrayList<Document>();
PImage table, shade, lamp, pap;
boolean lomp = false, ded = false;
Stamp S;
void setup() {
  size(1600, 900);
  imagePrep();
  prepValues();
  paint();
}
void draw() {
  holding();
  //stmp();
  paint();
}
void mouseMoved() {
  //holding();
}
void mousePressed() {
  mus.set(mouseX, mouseY);
  if (mouseButton == RIGHT)hold = -1;
  else if (hold > -2) {
    hold = grab();
    if(hold >= 0)D.get(hold).xyg = D.get(hold).xy;
  } else stmp();
  //holding();
  sortDoc();
}
void mouseDragged() {
  //holding();
}
void mouseReleased() {
  if (hold >= 0)D.get(hold).snap();
  if (hold > -2)hold = -1;
  end();
}
void end() {
  for (Document d : D)if (d.xy!=lockPoint[0])return;
  exit();
}
void setSaveFile() {
  JSONObject json = loadJSONObject("ww2jkmmwp/dokument.json");
  JSONObject stuff = json.getJSONObject("var");
  saveJSONObject(json, "ww2jkmmwp/sparfil.json");
  print(stuff);
}
