class Document {
  int id, stamps;
  PVector wh = new PVector(200, 300);
  PVector xy = new PVector(width-wh.x, wh.y);
  PVector xyg = new PVector();
  PImage sl;//paper, stamp layer
  //new PImage(int(this.wh.x*2),int(this.wh.y*2));
  color co;
  Document(int id) {
    this.sl = createImage(int(this.wh.x+1), int(this.wh.y+1), color(255, 255, 0, 255));
    colr(this.sl, color(255, 0, 0, 0));
    //this.pap.resize(int(this.wh.x*2),int(this.wh.y*2));
    this.id = id;
    //this.co = color(random(0, 255), random(0, 255), random(0, 255));
  }
  void snap() {
    for (PVector lp : lockPoint)this.xy = abs(lp.x-this.xy.x) < this.wh.x*.75 && abs(lp.y-this.xy.y) < this.wh.y*.75 ? lp : this.xy;
  }
  void stamp() {
    this.stamps++;
    int t = hold == -2 ? 0 : 1;
    this.sl.set(int(mouseX-this.xy.x+(this.wh.x-S.swh.x)/2), int(mouseY-this.xy.y+(this.wh.y-S.swh.y)/2), S.stmp[t]);
    if(this.stamps > 4)ded = true;
  }
  void paint() {
    //colr(this.pap, color(255,255,255,255));
    //fill(this.co);
    //stroke(0);
    //rect(this.xy.x, this.xy.y, this.wh.x, this.wh.y);
    pixlPaint(pap, this.xy, this.wh);
    //image(this.pap,this.xy.x, this.xy.y);
    image(this.sl, this.xy.x, this.xy.y);
    textSize(30);
    fill(0);
    text(this.id, this.xy.x, this.xy.y);
  }
}
class Stamp {
  PVector wh = new PVector(140, 140);
  PVector xy = new PVector(wh.x, height-wh.y);
  PVector swh = new PVector(this.wh.x/1.2, this.wh.y/2.4);
  PImage[] stmp = new PImage[2];
  Stamp() {
    this.stmp[0] = createImage(int(swh.x), int(swh.y), color(0, 0, 0));
    colr(this.stmp[0], color(255, 0, 0));
    this.stmp[1] = createImage(int(swh.x), int(swh.y), color(0, 0, 0));
    colr(this.stmp[1], color(0, 0, 255));
  }
  void paint() {
    fill(248, 142, 0);
    rect(this.xy.x, this.xy.y, this.wh.x, this.wh.y);
    fill(255, 0, 0);
    if (hold == -2)image(this.stmp[0], mouseX, mouseY);
    //rect(mouseX,mouseY,this.wh.x/1.2,this.wh.y/2.4);
    else rect(this.xy.x, this.xy.y-this.wh.y/4.8, this.wh.x/1.2, this.wh.y/2.4);
    fill(0, 255, 0);
    if (hold == -3)rect(mouseX, mouseY, this.wh.x/1.2, this.wh.y/2.4);
    else rect(this.xy.x, this.xy.y+this.wh.y/4.8, this.wh.x/1.2, this.wh.y/2.4);
  }
}
