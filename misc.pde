void imagePrep(){
  table = loadImage("img/table.png");
  shade = loadImage("img/shading.png");
  lamp = loadImage("img/lamp.png");
  pap = loadImage("img/doc1.png");
  textFont(createFont("BaksoSapi.otf",85));
  shade.loadPixels();
  for (int i = 0; i < shade.pixels.length; i++) {
    color c = shade.pixels[i];
    shade.pixels[i] = color(red(c), green(c), blue(c), alpha(c)/3);
  }
  shade.updatePixels();
  table.resize(width, height);
  shade.resize(width, height);
  lamp.resize(width, height);
  rectMode(CENTER);
  imageMode(CENTER);
  textAlign(CENTER,CENTER);
}
void prepValues(){
  centr = new PVector(width/2, height/2);
  S = new Stamp();
  for (int i = 0; i < 5; i++)D.add(new Document(i));
  lockPoint[0] = new PVector(D.get(0).wh.x, D.get(0).wh.y);
  lockPoint[1] = new PVector(width-D.get(0).wh.x, D.get(0).wh.y);
}
void stmp() {
  int a = grab();
  if(a >= 0)D.get(a).stamp();
}
void sortDoc() {
  if (hold <= 0 || hold > D.size()-1)return;
  Document temp = D.get(hold);
  D.remove(hold);
  D.add(0, temp);
  for (int i = 0; i < D.size(); i++)D.get(i).id = i;
  hold = 0;
}
int grab() {
  //if (hold < -1)return hold;
  if (abs(mouseX-S.xy.x)<S.wh.x/2&&abs(mouseY-S.xy.y)<S.wh.y/2)return mouseY-S.xy.y < 0 ? -2:-3;
  for (Document d : D)if (abs(mouseX-d.xy.x) < d.wh.x/2 && abs(mouseY-d.xy.y) < d.wh.y/2) {
    return(d.id);
  }
  return(-1);
}
void holding() {
  //noFill();
  if (hold >= 0 && hold <= D.size())D.get(hold).xy = D.get(hold).xyg.copy().sub(mus.copy().sub(new PVector(mouseX, mouseY)));
  /*switch(hold) {
   case(-2):
   stroke(255, 0, 0);
   rect(mouseX, mouseY, 160, 90);
   break;
   case(-3):
   stroke(0, 255, 0);
   rect(mouseX, mouseY, 160, 90);
   break;
   default:
   if (hold >= 0 && hold <= D.size())D.get(hold).xy = D.get(hold).xyg.copy().sub(mus.copy().sub(new PVector(mouseX, mouseY)));
   }*/
}
